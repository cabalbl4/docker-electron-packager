FROM ubuntu:bionic
WORKDIR /app
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y nodejs npm
RUN npm install -g electron-packager
RUN apt-get install -y zip
RUN apt-get install -y wine-stable
RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y wine32
COPY in /app/app
VOLUME /app/out
COPY make-dist.sh /app/make-dist.sh
ENTRYPOINT /app/make-dist.sh
